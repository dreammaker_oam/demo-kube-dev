package com.example.demokubedev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoKubeDevApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoKubeDevApplication.class, args);
	}

}
