package com.example.demokubedev;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo-kube-dev/v1")
@RefreshScope
public class HomeController {

    @Value("${hello.message}")
    private String message;

    @GetMapping("/hello")
    public String hello(){
        return message;
    }
}
